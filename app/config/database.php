<?php 

return [
	"connections" => [
		"mysql" => [
                  'driver' => 'mysql',
                  'host' => 'localhost',
                  'Database' => 'my-framework',
                  'username' => $_ENV["DB_USER"],
                  'password' =>  $_ENV["DB_PASS"],
                  'port' => '3306',
                  'charset' => 'utf8',
                  'collation' => 'utf8_unicode_ci',
                  'prefix' => '',
                  'strict' => true,
                  'engine' => null,
                  'options' => [
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                  ]
		]
	]
];