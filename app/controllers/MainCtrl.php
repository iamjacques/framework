<?php
namespace App\Controllers;
/**
 * Created by PhpStorm.
 * User: jacquesuwamungu
 * Date: 07/05/2017
 * Time: 05:12
 */
use App\Core\App;
use App\Core\Http\Request;

class MainCtrl
{
    /**
     *
     */
    public function index(){
        return view("index");
    }

    /**
     *
     */
    public function about(){
        return view("about");
    }

    /**
     *
     */
    public function contact(){
        return view("contact");
    }

    /**
     *
     */
    public function getsignup(){
        $all = App::get("Database")->all("users");
        return view("signup", compact("all"));
    }

    /**
     *
     */
    public function postsignup(){
        App::get("Database")->create("users", ["name"=>$_POST["name"]]);
        Request::redirect("/signup");
    }
}