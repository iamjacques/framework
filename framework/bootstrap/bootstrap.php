<?php
use App\Core\App;
use App\Core\Database\QueryBuilder;
use App\Core\Database\Connection;
use Symfony\Component\Dotenv\Dotenv;


//Use .env file
$dotenv = new Dotenv();
$dotenv->load(ROOT_DIR.'/../.env');

//Dependency Injection Container
App::bind("config", require dirname(ROOT_DIR)."/app/config/database.php" );
App::bind("Database", new QueryBuilder(
    Connection::connect(App::get("config")["connections"]["mysql"])
));

