<?php
namespace App\Core\Http;
/**
 * Created by PhpStorm.
 * User: jacquesuwamungu
 * Date: 07/05/2017
 * Time: 01:11
 */
class Request
{
    /**
     * @return string
     */
    public static function uri(){
        return trim(
            parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH)
            , "/"
        );
    }

    /**
     * @return bool
     */
    public static function isSecure(){

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on"  || $_SERVER['SERVER_PORT'] == 443 ){
            return true;
        }
        return false;
    }

    public static function rootUrl(){

    }


    public static function params(){

    }

    public static function param($key){

    }



    /**
     * @return method
     */
    public static function method(){
        return $_SERVER["REQUEST_METHOD"];
    }


    /**
     * @param $uri
     */
    public static function redirect($uri){
       header("Location: {$uri}");
    }
}